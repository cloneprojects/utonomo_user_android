package com.app.utonomo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.utonomo.R;

public class MoipPayment extends AppCompatActivity {
    public static final String EXTRA_CARD_NUMBER = "card_number";
    public static final String EXTRA_CVC = "cvc";
    public static final String EXTRA_EXPIRATION_YEAR = "expiration_year";
    public static final String EXTRA_EXPIRATION_MONTH = "expiration_month";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moip_payment);

        ImageView backButton = findViewById(R.id.backButton);
        Button loginButton = findViewById(R.id.loginButton);
        final EditText card_number = findViewById(R.id.card_number);
        final EditText expiration_month = findViewById(R.id.expiration_month);
        final EditText cvc = findViewById(R.id.cvc);
        final EditText expiration_year = findViewById(R.id.expiration_year);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String iCardNumber, iExpirationMonth, iCvc, iExpirationYear;


                if (card_number.length() == 0 &&
                        cvc.length() == 0 &&
                        expiration_month.length() == 0 && expiration_month.length() != 2 &&
                        expiration_year.length() == 0 && expiration_year.length() != 4) {

                    Toast.makeText(MoipPayment.this, R.string.valid_card_number, Toast.LENGTH_SHORT).show();

                } else if (card_number.length() == 0) {
                    Toast.makeText(MoipPayment.this, R.string.valid_card_number, Toast.LENGTH_SHORT).show();

                } else if (expiration_year.length() == 0 && expiration_year.length() != 4) {
                    Toast.makeText(MoipPayment.this, R.string.valid_expiration_year, Toast.LENGTH_SHORT).show();

                } else if (cvc.length() == 0) {
                    Toast.makeText(MoipPayment.this, R.string.valid_cvc, Toast.LENGTH_SHORT).show();

                } else if (expiration_month.length() == 0 && expiration_month.length() != 2) {
                    Toast.makeText(MoipPayment.this, R.string.valid_expiration_month, Toast.LENGTH_SHORT).show();

                } else {

                    iCardNumber = card_number.getText().toString();
                    iCvc = cvc.getText().toString();
                    iExpirationMonth = expiration_month.getText().toString();
                    iExpirationYear = expiration_year.getText().toString();

                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_CARD_NUMBER, iCardNumber);
                    intent.putExtra(EXTRA_CVC, iCvc);
                    intent.putExtra(EXTRA_EXPIRATION_MONTH, iExpirationMonth);
                    intent.putExtra(EXTRA_EXPIRATION_YEAR, iExpirationYear);

                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
}
