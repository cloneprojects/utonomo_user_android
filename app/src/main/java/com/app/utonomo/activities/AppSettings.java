package com.app.utonomo.activities;

import android.content.Context;

import com.app.utonomo.helpers.SharedHelper;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by user on 23-10-2017.
 */

public class AppSettings {
    String socialLogin;

    public void setIsSocialLogin(String socialLogin) {
        SharedHelper.putKey(context, "socialLogin", socialLogin);

        this.socialLogin = socialLogin;
    }

    public String getIsSocialLogin() {
        socialLogin = SharedHelper.getKey(context, "socialLogin");

        return socialLogin;
    }

    public String getImageUploadPath() {
        imageUploadPath = SharedHelper.getKey(context, "imageUploadPath");

        return imageUploadPath;
    }

    public void setImageUploadPath(String imageUploadPath) {
        SharedHelper.putKey(context, "imageUploadPath", imageUploadPath);

        this.imageUploadPath = imageUploadPath;
    }

    Context context;
    String token;
    String paymentType;
    String userImage;
    String imageUploadPath;

    public String getUserImage() {
        userImage = SharedHelper.getKey(context, "userImage");

        return userImage;
    }

    public void setUserImage(String userImage) {
        SharedHelper.putKey(context, "userImage", userImage);

        this.userImage = userImage;
    }

    public String getPaymentType() {
        paymentType = SharedHelper.getKey(context, "paymentType");

        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        SharedHelper.putKey(context, "paymentType", paymentType);

        this.paymentType = paymentType;
    }

    String selectedTimeSlot;
    String selectedSubCategory;

    public String getSelectedSubCategoryName() {
        selectedSubCategoryName = SharedHelper.getKey(context, "selectedSubCategoryName");

        return selectedSubCategoryName;
    }

    public void setSelectedSubCategoryName(String selectedSubCategoryName) {
        SharedHelper.putKey(context, "selectedSubCategoryName", selectedSubCategoryName);

        this.selectedSubCategoryName = selectedSubCategoryName;
    }

    String selectedSubCategoryName;
    String selectedDate;
    String selectedCity;
    String selectedlat;
    String userType;

    public String getSelectedAddress() {
        selectedAddress = SharedHelper.getKey(context, "selectedAddress");

        return selectedAddress;
    }

    public void setSelectedAddress(String selectedAddress) {
        SharedHelper.putKey(context, "selectedAddress", selectedAddress);

        this.selectedAddress = selectedAddress;
    }

    String selectedAddress;

    public String getIsLogged() {
        isLogged = SharedHelper.getKey(context, "isLogged");

        return isLogged;
    }

    public void setIsLogged(String isLogged) {
        SharedHelper.putKey(context, "isLogged", isLogged);

        this.isLogged = isLogged;
    }

    String isLogged;
    String selectedLong;

    public String getFireBaseToken() {
        fireBaseToken = SharedHelper.getKey(context, "fireBaseToken");
        return fireBaseToken;
    }

    public void setFireBaseToken(String fireBaseToken) {
        SharedHelper.putKey(context, "fireBaseToken", fireBaseToken);
        this.fireBaseToken = fireBaseToken;
    }

    public String getUserType() {
        userType = SharedHelper.getKey(context, "userType");

        return userType;
    }

    public void setUserType(String userType) {
        SharedHelper.putKey(context, "userType", userType);

        this.userType = userType;
    }

    String fireBaseToken;

    public String getSelectedAddressId() {
        selectedAddressId = SharedHelper.getKey(context, "selectedAddressId");

        return selectedAddressId;
    }

    public void setSelectedAddressId(String selectedAddressId) {
        SharedHelper.putKey(context, "selectedAddressId", selectedAddressId);

        this.selectedAddressId = selectedAddressId;
    }

    String selectedAddressId;

    public String getSelectedTimeText() {
        selectedTimeText = SharedHelper.getKey(context, "selectedTimeText");

        return selectedTimeText;
    }

    public void setSelectedTimeText(String selectedTimeText) {
        SharedHelper.putKey(context, "selectedTimeText", selectedTimeText);

        this.selectedTimeText = selectedTimeText;
    }

    String selectedTimeText;

    public String getSelectedAddressTitle() {
        selectedAddressTitle = SharedHelper.getKey(context, "selectedAddressTitle");

        return selectedAddressTitle;
    }

    public void setSelectedAddressTitle(String selectedAddressTitle) {
        SharedHelper.putKey(context, "selectedAddressTitle", selectedAddressTitle);

        this.selectedAddressTitle = selectedAddressTitle;
    }

    String selectedAddressTitle;
    JSONArray timeSlots;

    public AppSettings(Context context) {
        this.context = context;

    }

    public String getSelectedTimeSlot() {
        selectedTimeSlot = SharedHelper.getKey(context, "selectedTimeSlot");

        return selectedTimeSlot;
    }

    public void setSelectedTimeSlot(String selectedTimeSlot) {
        SharedHelper.putKey(context, "selectedTimeSlot", selectedTimeSlot);

        this.selectedTimeSlot = selectedTimeSlot;
    }

    public String getSelectedSubCategory() {
        selectedSubCategory = SharedHelper.getKey(context, "selectedSubCategory");

        return selectedSubCategory;
    }

    public void setSelectedSubCategory(String selectedSubCategory) {
        SharedHelper.putKey(context, "selectedSubCategory", selectedSubCategory);

        this.selectedSubCategory = selectedSubCategory;
    }

    public String getSelectedDate() {
        selectedDate = SharedHelper.getKey(context, "selectedDate");

        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        SharedHelper.putKey(context, "selectedDate", selectedDate);

        this.selectedDate = selectedDate;
    }

    public String getSelectedCity() {
        selectedCity = SharedHelper.getKey(context, "selectedCity");

        return selectedCity;
    }

    public void setSelectedCity(String selectedCity) {
        SharedHelper.putKey(context, "selectedCity", selectedCity);

        this.selectedCity = selectedCity;
    }

    public String getSelectedlat() {
        selectedlat = SharedHelper.getKey(context, "selectedlat");

        return selectedlat;
    }

    public void setSelectedlat(String selectedlat) {
        SharedHelper.putKey(context, "selectedlat", selectedlat);
        this.selectedlat = selectedlat;
    }

    public String getSelectedLong() {
        selectedLong = SharedHelper.getKey(context, "selectedLong");
        return selectedLong;
    }

    public void setSelectedLong(String selectedLong) {
        SharedHelper.putKey(context, "selectedLong", selectedLong);
        this.selectedLong = selectedLong;
    }

    public String getToken() {
        token = SharedHelper.getKey(context, "token");
        return token;
    }

    public void setToken(String token) {
        SharedHelper.putKey(context, "token", token);
        this.token = token;
    }

    public JSONArray getTimeSlots() {
        try {
            timeSlots = new JSONArray(SharedHelper.getKey(context, "timeSlots"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return timeSlots;
    }

    public void setTimeSlots(JSONArray timeSlots) {
        SharedHelper.putKey(context, "timeSlots", timeSlots.toString());
        this.timeSlots = timeSlots;
    }
}
